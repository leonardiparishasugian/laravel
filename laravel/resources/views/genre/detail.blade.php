@extends('layout.master')
@section('title')
    Halaman Detail Genre
@endsection
@section('subtitle')
    Genre
@endsection
@section('content')
    {{-- <h1>Daftar Film Genre {{$genre->nama}}</h1> --}}
    <div class="row">
        @forelse ($genre->film as $item)
            <div class="col-3">
                <div class="card">
                    <img src="{{asset('image/' . $item->poster)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h4>{{$item->judul}}</h4>
                        <p class="card-text">{{Str::limit($item->ringkasan,125)}}</p>
                        <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Lanjutkan Membaca</a>                       
                    </div>
                </div>
            </div>
        @empty
            
        @endforelse
    </div>

        {{-- <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Judul</th>
                <th scope="col">Tahun</th>
                <th scope="col">Poster</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($genre->film as $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->judul}}</td>
                        <td>{{$item->tahun}}</td>
                        <td>{{$item->poster}}</td>
                    </tr>
                @empty
                    <tr>
                        <td>Data Masih Kosong</td>
                    </tr>
                @endforelse
            </tbody>
          </table> --}}

    <a href="/genre" class="btn btn-primary my-2">Kembali</a>
@endsection