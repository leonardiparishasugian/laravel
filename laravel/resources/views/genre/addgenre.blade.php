@extends('layout.master')
@section('title')
    Halaman Tambah Genre
@endsection
@section('subtitle')
    Cast
@endsection
@section('content')
<form action="/genre" method="POST">
  @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/genre" class="btn btn-primary my-2">Kembali</a>
  </form>
@endsection