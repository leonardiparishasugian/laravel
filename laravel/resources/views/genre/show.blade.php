@extends('layout.master')
@section('title')
    Halaman Tampil Genre
@endsection
@section('subtitle')
    Genre
@endsection
@section('content')
<form action="/genre" method="POST">
    <a href="/genre/create" class="btn btn-primary my-2">Tambah Genre</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Nama</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($genre as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/genre/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Hapus">

                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
      </table>
@endsection