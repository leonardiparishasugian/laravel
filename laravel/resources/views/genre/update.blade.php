@extends('layout.master')
@section('title')
    Halaman Update Genre
@endsection
@section('subtitle')
    Genre
@endsection
@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$genre->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection