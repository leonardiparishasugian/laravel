    @extends('layout.master')
    @section('title')
        Halaman Konfirmasi Registrasi
    @endsection
    @section('subtitle')
        Registrasi Berhasil
    @endsection
    @section('content')
    <h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}}!</h1>
    <h2>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h2>
    @endsection