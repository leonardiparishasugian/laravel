    @extends('layout.master')
    @section('title')
        Halaman Registrasi
    @endsection
    @section('subtitle')
        Registrasi
    @endsection
    @section('content')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br>
        <input type="radio" name="gender" value="Other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="ina">Indonesian</option>
            <option value="sgr">Singapuran</option>
            <option value="mly">Malaysian</option>
            <option value="aus">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language" value="ID">Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="EN">English<br>
        <input type="checkbox" name="language" value="O">Other<br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea id="bio" name="bio" cols="30%" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
    @endsection