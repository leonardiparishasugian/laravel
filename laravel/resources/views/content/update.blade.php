@extends('layout.master')
@section('title')
    Halaman Update Cast
@endsection
@section('subtitle')
    Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control" min="1" max="150" name="umur" value="{{$cast->umur}}>
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea class="form-control" name="bio" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection