@extends('layout.master')
@section('title')
    Halaman Detail Cast
@endsection
@section('subtitle')
    Cast
@endsection
@section('content')
    <h1>{{$cast->nama}}</h1>
    <p>
        Umur {{$cast->umur}} tahun<br>
        {{$cast->bio}}
    </p><br>
    <a href="/cast" class="btn btn-primary my-2">Kembali</a>
@endsection