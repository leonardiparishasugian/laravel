@extends('layout.master')
@section('title')
    Halaman Tampil Cast
@endsection
@section('subtitle')
    Cast
@endsection
@section('content')
<form action="/cast" method="POST">
    <a href="/cast/create" class="btn btn-primary my-2">Tambah Cast</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Nama</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/cast/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Hapus">

                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
      </table>
@endsection