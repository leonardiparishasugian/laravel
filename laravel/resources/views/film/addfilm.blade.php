@extends('layout.master')
@section('title')
    Halaman Tambah Film
@endsection
@section('subtitle')
    Film
@endsection
@section('content')
<form action="/film" method="POST" enctype="multipart/form-data">
  @csrf
    <div class="form-group">
      <label>Judul</label>
      <input type="text" class="form-control" name="judul">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ringkasan</label>
      <textarea class="form-control" name="ringkasan" cols="30" rows="10"></textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Tahun</label>
      <input type="number" class="form-control" min="1800" max="2023" name="tahun">
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Poster</label>
      <input type="file" class="form-control" name="poster">
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Genre</label>
      <select name="genre_id" class="form-control" id="">
        <option value="">--Pilih Genre--</option>
        @forelse ($genre as $item)
          <option value="{{$item->id}}">{{$item->nama}}</option>
        @empty
            <option value="">Tidak ada Data Genre</option>
        @endforelse
      </select>
    </div>
    @error('genre_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/film" class="btn btn-primary my-2">Kembali</a>
  </form>
@endsection