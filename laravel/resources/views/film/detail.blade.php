@extends('layout.master')
@section('title')
    Halaman Detail Film
@endsection
@section('subtitle')
    Film
@endsection
@section('content')
    {{-- <h1>{{$film->judul}}</h1>
    <p>
        <b>Ringkasan</b><br>
        {{$film->ringkasan}}<br><br>
        <b>Tahun</b><br>
        {{$film->tahun}}<br><br>
        <b>Poster</b><br>
        {{$film->poster}}<br><br>
        {{-- <b>Genre</b><br>
        {{$genre->fileName}}<br><br>
    </p><br>--}}

    
    <div class="card">
        <img src="{{asset('image/' . $film->poster)}}" class="card-img-top" alt="...">
        <div class="card-body">
            <h4>{{$film->judul}}</h4>
            <p class="card-text">Tahun Release {{$film->tahun}}</p>
            <p class="card-text">{{$film->ringkasan}}</p>
        </div>
    </div>
    
    <hr>
    @forelse ($film->kritik as $item)
    <div class="card">
        <div class="card-header">
             {{$item->user->name}}
        </div>
        <div class="card-body">
            <h5>Rating {{$item->point}} Poin</h5>
            <p class="card-text">{{$item->content}}</p>
        </div>
    </div>
    @empty
        <h3>Tidak Ada Ulasan</h3>
    @endforelse
    <hr>
    <div>
        <form action="/kritik/{{$film->id}}" method="POST">
            @csrf
            <select name="point" class="form-control" id="">
                <option value="">Pilih Rating</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <textarea name="content" class="form-control" placeholder="Tulis Ulasan" id="" cols="20" rows="5"></textarea>
            <input type="submit" class="btn btn-primary my-2" value="Submit">
            <a href="/film" class="btn btn-primary my-2">Kembali</a>
        </form>
    </div>
    {{-- <a href="/film" class="btn btn-primary my-2">Kembali</a> --}}
@endsection