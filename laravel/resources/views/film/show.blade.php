@extends('layout.master')
@section('title')
    Halaman Tampil Film
@endsection
@section('subtitle')
    Film
@endsection
@section('content')
{{-- <form action="/film" method="POST">
    @auth
        <a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>        
    @endauth
    <table class="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Judul</th>
            <th scope="col">Tahun</th>
            <th scope="col">Poster</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($film as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->judul}}</td>
                    <td>{{$item->tahun}}</td>
                    <td>{{$item->poster}}</td>
                    <td>
                        <form action="/film/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            @auth
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                            @endauth
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
      </table> --}}

    @auth
    <a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>
    @endauth
    <div class="row">
        @forelse ($film as $item)
            <div class="col-3">
                <div class="card">
                    <img src="{{asset('image/' . $item->poster)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h4>{{$item->judul}}</h4>
                        <p class="card-text">{{Str::limit($item->ringkasan,125)}}</p>
                        <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Lanjutkan Membaca</a>
                        @auth
                        <div class="row my-2">
                            <div class="col">
                                <a href="/film/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit</a>
                            </div>
                            <div class="col">
                                <form action="/film/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                                </form>
                            </div>
                        </div>                            
                        @endauth
                    </div>
                </div>
            </div>            
        @empty
            <h2>Tidak ada Data Film</h2>
        @endforelse
        
    </div>





    {{-- <div class="row">
        @forelse
        <div class="card">
            <img src="{{asset('image/'.$item->image)}}" class="card-img-top" alt="..." height="150px" width="150px">
            <div class="card-body">
                <h2>{{$item->judul}}</h2>
                <p class="card-text">{{Str::limit($item->ringkasan, 20)}}</p>
                <a href="/film/{{$item->id}}" class="btn btn-primary btn-block btn-sm">Baca</a>
                <div class="row my-2">
                    @auth
                    <div class="col">
                        <a href="/film/{{$item->id}}/edit class="btn btn-info btn-block btn-sm">Edit</a>
                    </div>
                    <div class="col">
                        <form action="/film/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                        </form>    
                    @endauth
                    </div>
                </div>
            </div>
        </div>
        @empty
        <h2>Tidak ada Data Film</h2>
        @endforelse
    </div> --}}
@endsection