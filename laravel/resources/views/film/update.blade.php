@extends('layout.master')
@section('title')
    Halaman Update Film
@endsection
@section('subtitle')
    Film
@endsection
@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Judul</label>
      <input type="text" class="form-control" name="judul" value="{{$film->judul}}">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ringkasan</label>
      <textarea class="form-control" name="ringkasan" cols="30" rows="10">{{$film->ringkasan}}</textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Tahun</label>
      <input type="number" class="form-control" min="1800" max="2023" name="tahun" value="{{$film->tahun}}">
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Poster</label>
      <input type="file" class="form-control" name="poster">
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Genre</label>
      <select name="genre_id" class="form-control" id="">
        <option value="">--Pilih Genre--</option>
        @forelse ($genre as $item)
          @if ($item->id === $film->genre_id)
            <option value="{{$item->id}}" selected>{{$item->nama}}</option>              
          @else
            <option value="{{$item->id}}">{{$item->nama}}</option>
          @endif
        @empty
            <option value="">Tidak ada Data Genre</option>
        @endforelse
      </select>
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection