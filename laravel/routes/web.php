<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\KritikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class,'home']);
Route::get('/regis', [AuthController::class,'reg']);
Route::post('/welcome', [AuthController::class,'wel']);
Route::get('/home', [AuthController::class,'backhome']);
Route::get('/dtable', [IndexController::class,'table']);

// Testing tampilan template master
// Route::get('/master', function(){
//     return view('layout.master');
// });

Route::group(['middleware' => ['auth']], function () {
    //CRUD
    //CREATE Data
    Route::get('/cast/create',[CastController::class,'create']); //Route untuk mengarah ke form tambah cast
    Route::post('/cast',[CastController::class,'store']); //Simpan data cast pada dbmovie
    //READ Data
    Route::get('/cast',[CastController::class,'index']); //Tampil semua data pada tabel
    Route::get('/cast/{cast_id}',[CastController::class,'show']); //Detail cast berdasarkan id
    //UPDATE Data
    Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']); //Route untuk mengarah ke form edit cast berdasarkan id
    Route::put('/cast/{cast_id}',[CastController::class,'update']); //Simpan data cast pada dbmovie setelah di edit
    //DELETE Data
    Route::delete('/cast/{cast_id}',[CastController::class,'destroy']);
});

//Kuis
Route::get('/game/create',[GameController::class,'create']);
Route::post('/game',[GameController::class,'store']);
Route::get('/game',[GameController::class,'index']);
Route::get('/game/{game_id}',[GameController::class,'show']);
Route::get('/game/{game_id}/edit',[GameController::class,'edit']);
Route::get('/game/{game_id}',[GameController::class,'update']);
Route::delete('/game/{game_id}',[GameController::class,'destroy']);

Route::group(['middleware' => ['auth']], function () {

    //TA Genre
    Route::get('/genre/create',[GenreController::class,'create']); //Create: Route ke form tambah genre
    Route::post('/genre',[GenreController::class,'store']); //Create: Simpan data genre
    Route::get('/genre',[GenreController::class,'index']); //Read: Tampil semua data pada tabel
    Route::get('/genre/{genre_id}',[GenreController::class,'show']); //Read: Detail film berdasarkan id
    Route::get('/genre/{genre_id}/edit',[GenreController::class,'edit']); //Update: Route ke form edit genre berdasarkan id
    Route::put('/genre/{genre_id}',[GenreController::class,'update']); //Update: Simpan data genre
    Route::delete('/genre/{genre_id}',[GenreController::class,'destroy']); //Delete: Hapus data

    //TA Kritik
    Route::post('/kritik/{film_id}',[KritikController::class,'store']);
});

//Route::group(['middleware' => ['auth']], function () {
    //TA Film
    Route::get('/film/create',[FilmController::class,'create']); //Create: Route ke form tambah film
    Route::post('/film',[FilmController::class,'store']); //Create: Simpan data film
    Route::get('/film',[FilmController::class,'index']); //Read: Tampil semua data pada tabel
    Route::get('/film/{film_id}',[FilmController::class,'show']); //Read: Detail film berdasarkan id
    Route::get('/film/{film_id}/edit',[FilmController::class,'edit']); //Update: Route ke form edit film berdasarkan id
    Route::put('/film/{film_id}',[FilmController::class,'update']); //Update: Simpan data film
    Route::delete('/film/{film_id}',[FilmController::class,'destroy']); //Delete: Hapus data
//});

    // Route::resource('film',FilmController::class);

Auth::routes();