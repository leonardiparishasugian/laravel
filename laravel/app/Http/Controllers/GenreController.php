<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Genre;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genre = Genre::all();
        // $genre = DB::table('genre')->get();
        return view('genre.show',['genre'=>$genre]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('genre.addgenre');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required'
        ]);

        Genre::create([
            'nama'=>$request['nama']          
        ]);
        // DB::table('genre')->insert([
        //     'nama'=>$request['nama']
        // ]);

        return redirect('/genre');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = Genre::find($id);
        //$genre = DB::table('genre')->where('id',$id)->first();
        return view('genre.detail',['genre'=>$genre]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = Genre::find($id);
        // $genre = DB::table('genre')->where('id',$id)->first();
        return view('genre.update',['genre'=>$genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required'
        ]);
        
        Genre::where('id',$id)
            ->update([
                'nama' => $request['nama']              
            ]);
        // DB::table('genre')
        //     ->where('id',$id)
        //     ->update(
        //         ['nama'=>$request->nama],
        //     );

        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genre = Genre::find($id);
        $genre->delete();
        // DB::table('genre')->where('id',$id)->delete();

        return redirect('/genre');
    }
}