<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Genre;
use App\Models\Film;
use File;

class FilmController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth')->except(['index','show']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $film = Film::all();
        $film = Film::get();
        return view('film.show',['film'=>$film]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::get();
        return view('film.addfilm',['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpg,jpeg,png,gif',
            'genre_id' => 'required'
        ]);

        $fileName = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('image'),$fileName);

        $film = new Film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $fileName;
        $film->genre_id = $request->genre_id;

        $film->save();

        // Film::create([
        //     'judul' => $request['judul'],
        //     'ringkasan' => $request['ringkasan'],
        //     'tahun' => $request['tahun'],
        //     'poster' => $request['poster'],
        //     'genre_id' => $request['genre_id']
        //     //'genre_id' => auth()->id()          
        // ]);

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('film.detail',['film'=>$film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::get();
        return view('film.update',['film'=>$film, 'genre'=>$genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpg,jpeg,png',
            'genre_id' => 'required'
        ]);
        
        // Film::where('id',$id)
        //     ->update([
        //         'judul' => $request['judul'],
        //         'ringkasan' => $request['ringkasan'],
        //         'tahun' => $request['tahun'],
        //         'poster' => $request['poster'],
        //         'genre_id' => $request['fileName']
        //     ]);
        $film = Film::find($id);
        if($request->has('poster')){
            $path = 'image/';
            File::delete($path. $film->poster);
            $fileName = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('image'),$fileName);
            $film->poster = $fileName;
            $film->save();
        }
        
        $film->judul = $request['judul'];
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->genre_id = $request['genre_id'];
        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
        $film->delete();

        return redirect('/film');
    }
}