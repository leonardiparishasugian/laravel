<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg()
    {
        return view('page.regis');
    }

    public function wel(Request $request)
    {
        $namaDepan = $request['firstname'];
        $namaBelakang = $request['lastname'];
        return view('page.welcome',['namaDepan'=>$namaDepan, 'namaBelakang'=>$namaBelakang]);
    }

    public function backhome()
    {
        return view('home');
    }
}