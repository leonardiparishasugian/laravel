<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Kritik;

class KritikController extends Controller
{
    public function store(Request $request, $id){
        $request->validate([
            'point' => 'required',
            'content' => 'required'
        ]);

        Kritik::create([
            'point' => $request['point'],
            'content' => $request['content'],
            'user_id' => Auth::id(),
            'film_id' => $id
        ]);

        return redirect('/film/'. $id);
    }
}
