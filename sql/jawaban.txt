1. Create database
CREATE DATABASE library;

2. Create table
CREATE TABLE users(
    id integer PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255)
    );

CREATE TABLE categories(
    id integer PRIMARY KEY AUTO_INCREMENT,
    name varchar(255)
    );

CREATE TABLE books(
    id integer PRIMARY KEY AUTO_INCREMENT,
    title varchar(255),
    summary text,
    stock integer,
    category_id integer,
    FOREIGN KEY(category_id) REFERENCES categories(id)
    );

3. Insert data
INSERT INTO `users`(`name`, `email`, `password`) VALUES ('John Doe','john@doe.com','john123'),('Jane Doe','jane@doe.com','jenita123');

INSERT INTO `categories`(`name`) VALUES ('Novel'),('Manga'),('Comic'),('History'),('IT');

INSERT INTO `books`(`title`, `summary`, `stock`, `category_id`) VALUES ('One Piece','The series focuses on Monkey D Luffy, a young man made of rubber',50,2), ('Laskar Pelangi',"Belitung is known for its richness in tin, making it one of Indonesia's richest islands",25,1),('Leonardi Paris Hasugian',"Mitsuha Miyamizu, a high school girl living in the fictional town of Itomori in Gifu Prefecture's",15,2);

4. Select data
a.
SELECT name, email FROM `users`;

b.
SELECT * FROM `books` WHERE stock>=20;
SELECT * FROM `books` WHERE title LIKE '%pela%';

c.
SELECT books.title, books.summary, books.stock, books.category_id, categories.name AS category FROM `books` INNER JOIN categories on books.category_id = categories.id;

5. Update data
UPDATE `books` SET `stock`='30' WHERE title="One Piece";